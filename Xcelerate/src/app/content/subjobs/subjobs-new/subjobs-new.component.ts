import { Component, OnInit } from '@angular/core';
import {Router,ActivatedRoute} from '@angular/router'
import {HttpClient,HttpHeaders, HttpParams, HttpErrorResponse} from '@angular/common/http';
import {TitleTextService} from '../../../shared/services/titletext.service';
import {JobImagesList} from '../../../models/jobimages-new';
import {APIUrls} from '../../../shared/constants/apiurls';
import {ContactDetails} from '../../../models/contact-details';
import {Messages} from '../../../shared/constants/messages';
import {ResponseMessage} from '../../../models/response-message';
import {GrowlModule,Message} from 'primeng/primeng';
import { RolesInfo } from '../../../models/roles';
import * as jsPDF from 'jspdf';
import * as html2canvas from 'html2canvas';

@Component({
  selector: 'subjob-new',
  templateUrl: './subjobs-new.component.html',
  styleUrls: ['./subjobs-new.component.css']
})


export class SubJobsNewComponent implements OnInit {
  disabled: boolean = false;
  imagearray :JobImagesList[];
  otherimagedsbl: boolean = false;
  displayImageName:string ='';
  url:string;
  sub:any;
  id:string;
  parameterID:string;
  myData:any;
  masterNumberData:any;
  allSupervisors:ContactDetails[];
  allEstimators:ContactDetails[];
  allCoordinators: ContactDetails[];
  financeContacts:ContactDetails[];
  filteredSupervisors:ContactDetails[];
  filteredEstimators:ContactDetails[];
  filteredCoordinators:ContactDetails[];
  filteredFinance:ContactDetails[];
  allSales:ContactDetails[];
  filteredSalesName:ContactDetails[];
  filteredAdministrator:ContactDetails[];
  filteredBlankRole:ContactDetails[];
  blankRoles:ContactDetails[];
  blankRoles1:ContactDetails[];
  blankRoles2:ContactDetails[];
  isError:boolean;
  serverErrorMessage:string;
  responseMessage:any;
  msgs: Message[] = [];
  fileDetails: any;
  saveContactList: any;
  supervisor: any;
  coordinator: any;
  finance: any;
  estimator: any;
  salesRepresentativeName:any;
  newBlankRole:any;
  Administrator:any;
  role:any;
  admin: string="Administrator";
  Role: string="Role";
  display: boolean = false;
  displayEmail: boolean = false;
  description: string;
  contactDetails: any;
  action: string;
  roles: RolesInfo[];
  emailTo: string;
    emailSubject: string;
    emailSource: string='Job';
    emailOption: string='File Details';
    emailPattern =/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9\-]+\.)+([a-zA-Z0-9\-\.]+)+([;]([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9\-]+\.)+([a-zA-Z0-9\-\.]+))*$/;
    showMailToMsg: boolean = false;
    showSubjectMsg: boolean = false;
  showDialog(contactType){
      this.contactDetails={};
      this.contactDetails.contactType=contactType;
      this.display = true;
  }
  showDialog1(){
    this.emailTo = "";
    this.showMailToMsg = false;
    this.showSubjectMsg = false;
    this.emailSubject = this.fileDetails.jobCode +" Details";
    this.displayEmail = true;
  }
  constructor(private data: TitleTextService,private router: Router, private route: ActivatedRoute, private http: HttpClient) {
  }
 
  onImgClick(itemNumber){
    
   //alert('Clicked imaged Index Value :' + itemNumber);
    
    for (let i = 0; i < this.imagearray.length;i++ )
    {
       this.imagearray[i].imageActive=false;
    }
    this.imagearray[itemNumber].imageActive=true;
    this.displayImageName = this.imagearray[itemNumber].jobName;
  }

  /* Supervisors */
  getSupervisors(){
    this.url =APIUrls.hosturl+APIUrls.NewFileSupervisor;
    this.http.get(this.url)
        .subscribe(data=>{
          this.myData=data;
          this.allSupervisors=this.myData.contacts;
          for (let i=0;i<this.allSupervisors.length;i++){
            this.allSupervisors[i].fullName=this.allSupervisors[i].firstName+" , "+this.allSupervisors[i].lastName;
          }
        },
        (err: HttpErrorResponse) => {
            if (err.error instanceof Error) {
              console.log("Client-side error occured.");
            } else {
              console.log("Server-side error occured.",err);
            }
          }
        );
  }

  /* Estimators */
  getEstimators(){
    this.url =APIUrls.hosturl+APIUrls.NewFileEstimator;
    this.http.get(this.url)
        .subscribe(data=>{
          this.myData=data;
          this.allEstimators=this.myData.contacts;
          for (let i=0;i<this.allEstimators.length;i++){
            this.allEstimators[i].fullName=this.allEstimators[i].firstName+" , "+this.allEstimators[i].lastName;
          }
        },
        (err: HttpErrorResponse) => {
            if (err.error instanceof Error) {
              console.log("Client-side error occured.");
            } else {
              console.log("Server-side error occured.",err);
            }
          }
        );
  }

  /* Coordinators */
  getCoordinators(){
    this.url =APIUrls.hosturl+APIUrls.NewFileCoordinator;
    this.http.get(this.url)
        .subscribe(data=>{
          this.myData=data;
          this.allCoordinators=this.myData.contacts;
          for (let i=0;i<this.allCoordinators.length;i++){
            this.allCoordinators[i].fullName=this.allCoordinators[i].firstName+" , "+this.allCoordinators[i].lastName;
          }
        },
        (err: HttpErrorResponse) => {
            if (err.error instanceof Error) {
              console.log("Client-side error occured.");
            } else {
              console.log("Server-side error occured.",err);
            }
          }
        );
  }

  /* Finance */
  getFinance(){
    this.url =APIUrls.hosturl+APIUrls.NewFileFinance;
    this.http.get(this.url)
        .subscribe(data=>{
          this.myData=data;
          this.financeContacts=this.myData.contacts;
          for (let i=0;i<this.financeContacts.length;i++){
            this.financeContacts[i].fullName=this.financeContacts[i].firstName+" , "+this.financeContacts[i].lastName;
          }
        },
        (err: HttpErrorResponse) => {
            if (err.error instanceof Error) {
              console.log("Client-side error occured.");
            } else {
              console.log("Server-side error occured.",err);
            }
          }
        );
  }

  /* Slaes Reperentative */
  getSales(){
    this.url =APIUrls.hosturl+APIUrls.MasterNumberSalesRepresentative;
    this.http.get(this.url)
        .subscribe(data=>{
          this.myData=data;
          this.allSales=this.myData.contacts;
          for (let i=0;i<this.allSales.length;i++){
            this.allSales[i].fullName=this.allSales[i].firstName+" , "+this.allSales[i].lastName;
          }
        },
        (err: HttpErrorResponse) => {
            if (err.error instanceof Error) {
              console.log("Client-side error occured.");
            } else {
              console.log("Server-side error occured.",err);
            }
          }
        );
  }

  /* Blank Role1 */
  getAdministrator(){
    let getParams = new HttpParams().set('role', this.admin);
    this.url =APIUrls.hosturl+APIUrls.NewFileRoleContacts;
    this.http.get(this.url,{params:getParams})
        .subscribe(data=>{
          this.myData=data;
          this.blankRoles1=this.myData.contacts;
          for (let i=0;i<this.blankRoles1.length;i++){
            this.blankRoles1[i].fullName=this.blankRoles1[i].firstName+" , "+this.blankRoles1[i].lastName;
          }
        },
        (err: HttpErrorResponse) => {
            if (err.error instanceof Error) {
              console.log("Client-side error occured.");
            } else {
              console.log("Server-side error occured.",err);
            }
          }
        );
  }

/* Blank Role2 */
  getBlankRole(){
    let getParams = new HttpParams().set('role', this.Role);
    this.url =APIUrls.hosturl+APIUrls.NewFileRoleContacts;
    this.http.get(this.url,{params:getParams})
        .subscribe(data=>{
          this.myData=data;
          this.blankRoles2=this.myData.contacts;
          for (let i=0;i<this.blankRoles2.length;i++){
            this.blankRoles2[i].fullName=this.blankRoles2[i].firstName+" , "+this.blankRoles2[i].lastName;
          }
        },
        (err: HttpErrorResponse) => {
            if (err.error instanceof Error) {
              console.log("Client-side error occured.");
            } else {
              console.log("Server-side error occured.",err);
            }
          }
        );
  }
  

  ngOnInit(): void {
    this.contactDetails={};
    this.sub = this.route.params.subscribe(params => {
     this.id = params['id'];
    }); 
     this.parameterID=this.id;
     let getParams = new HttpParams().set('mastNumHash', this.parameterID);
     this.url =APIUrls.hosturl+APIUrls.MasterNumberDetails
    this.http.get(this.url,{params:getParams})
      .subscribe(data=>{
          this.myData = data;
          this.masterNumberData=this.myData.masterNumberDetails;
          this.data.changeMessage("Master Job Number: "+"   "+this.masterNumberData.mastNumCode);
         },
        (err: HttpErrorResponse) => {
            this.isError=true;
            this.serverErrorMessage = Messages.ServerErrorMessage;
            if (err.error instanceof Error) {
              console.log("Client-side error occured.");
            } else {
              console.log("Server-side error occured.");
            }
          }
        );
      this.getEstimators();
      this.getSupervisors();
      this.getCoordinators();
      this.getFinance();
      this.getSales();
      this.getAdministrator();
      this.getBlankRole();
      this.imagearray = [
                          {
                            jobActiveImage :'./assets/images/Water_r.png',
                            jobInactiveImage:'./assets/images/Water_.png',
                            imageActive:false,
                            jobName:'Water',
                          },
                          {
                            jobActiveImage :'./assets/images/Reconstruction_r.png',
                            jobInactiveImage :'./assets/images/Reconstruction_.png',
                            imageActive:false,
                            jobName:'Reconstruction',
                          },
                          {
                            jobActiveImage :'./assets/images/Content_r.png',
                            jobInactiveImage :'./assets/images/Content_.png',
                            imageActive:false,
                            jobName:'Content',
                          },
                          {
                            jobActiveImage :'./assets/images/Mold_r.png',
                            jobInactiveImage :'./assets/images/Mold_.png',
                            imageActive:false,
                            jobName:'Mold',
                          },
                          {
                            jobInactiveImage :'./assets/images/Asbestos_.png',
                            jobActiveImage :'./assets/images/Asbestos_r.png',
                            imageActive:false,
                            jobName:'Asbestos',
                          },
                          {
                            jobInactiveImage :'./assets/images/Board_.png',
                            jobActiveImage :'./assets/images/Board Up_r.png',
                            imageActive:false,
                            jobName:'Board Up',
                          },
                          {
                            jobInactiveImage :'./assets/images/Consulting_.png',
                            jobActiveImage :'./assets/images/Consulting_r.png',
                            imageActive:false,
                            jobName:'Consulting',
                          },
                          {
                            jobInactiveImage :'./assets/images/Bio_.png',
                            jobActiveImage :'./assets/images/Bio_r.png',
                            imageActive:false,
                            jobName:'Bio',
                          },
                        ]

    }

    filterSupervisor(event){
      this.filteredSupervisors=[];
      for(let i = 0; i < this.allSupervisors.length; i++) {
          let supervisor = this.allSupervisors[i];
          if(supervisor.fullName.toLowerCase().indexOf(event.query.toLowerCase()) !==-1){
              this.filteredSupervisors.push(supervisor);
          }
      }
     }

     filterEstimator(event){
      this.filteredEstimators=[];
      for(let i = 0; i < this.allEstimators.length; i++) {
          let estimator = this.allEstimators[i];
          if(estimator.fullName.toLowerCase().indexOf(event.query.toLowerCase()) !==-1){
              this.filteredEstimators.push(estimator);
          }
      }
     }

     filterCoordinator(event){
      this.filteredCoordinators=[];
      for(let i = 0; i < this.allCoordinators.length; i++) {
          let coordinator = this.allCoordinators[i];
          if(coordinator.fullName.toLowerCase().indexOf(event.query.toLowerCase()) !==-1){
              this.filteredCoordinators.push(coordinator);
          }
      }
     }

     filterFinance(event){
      this.filteredFinance=[];
      for(let i = 0; i < this.financeContacts.length; i++) {
          let finance = this.financeContacts[i];
          if(finance.fullName.toLowerCase().indexOf(event.query.toLowerCase()) !==-1){
              this.filteredFinance.push(finance);
          }
      }
     }

     filterSaleName(event){
      this.filteredSalesName=[];
      for(let i = 0; i < this.allSales.length; i++) {
          let agent = this.allSales[i];
          if(agent.fullName.toLowerCase().indexOf(event.query.toLowerCase()) == 0){
              this.filteredSalesName.push(agent);
          }
      }
    }

    filterAdministrator(event){
      this.filteredAdministrator=[];
      for(let i = 0; i < this.blankRoles1.length; i++) {
          let blankRole1 = this.blankRoles1[i];
          if(blankRole1.fullName.toLowerCase().indexOf(event.query.toLowerCase()) == 0){
              this.filteredAdministrator.push(blankRole1);
          }
      }
    }

    filterBlankRole(event){
      this.filteredBlankRole=[];
      for(let i = 0; i < this.blankRoles2.length; i++) {
          let blankRole2 = this.blankRoles2[i];
          if(blankRole2.fullName.toLowerCase().indexOf(event.query.toLowerCase()) == 0){
              this.filteredBlankRole.push(blankRole2);
          }
      }
    }

     saveData(f){
       if(this.displayImageName!=undefined && this.displayImageName!=null && this.displayImageName!=""){
          this.fileDetails={};
          this.saveContactList=[];
          this.fileDetails.jobType=this.displayImageName;
          this.fileDetails.mastNumId=this.masterNumberData.mastNumId;
         // this.fileDetails.jobDescription=this.description;
          if (this.supervisor !== undefined && this.supervisor !== null && this.supervisor.id !== undefined){
            this.saveContactList.unshift({"contactId":this.supervisor.id, "userId":"-1" , "roleName":"Supervisor"});
          }
          if (this.estimator !== undefined && this.estimator !== null && this.estimator.id !== undefined){
            this.saveContactList.unshift({"contactId":this.estimator.id, "userId":"-1" , "roleName":"Estimator"});
          }
          if (this.coordinator !== undefined && this.coordinator !== null && this.coordinator.id !== undefined){
            this.saveContactList.unshift({"contactId":this.coordinator.id, "userId":"-1" , "roleName":"Coordinator"});
          }
          if (this.finance !== undefined && this.finance !== null && this.finance.id !== undefined){
            this.saveContactList.unshift({"contactId":this.finance.id, "userId":"-1" , "roleName":"Finance"});
          }
          if (this.salesRepresentativeName !== undefined && this.salesRepresentativeName !== null && this.salesRepresentativeName.id !== undefined){
            this.saveContactList.unshift({"contactId":this.salesRepresentativeName.id, "userId":"-1" , "roleName":"Sales Representative"});
          }
          if (this.Administrator !== undefined && this.Administrator !== null && this.Administrator.id !== undefined){
            this.saveContactList.unshift({"contactId":this.Administrator.id, "userId":"-1" , "roleName":"Administrator"});
          }
          if (this.role !== undefined && this.role !== null && this.role.id !== undefined){
            this.saveContactList.unshift({"contactId":this.role.id, "userId":"-1" , "roleName":"Role"});
          }

          if(this.saveContactList.length>0){
            this.fileDetails.roles=this.saveContactList;
          }
          this.saveFileDetails(f);
          //f.reset();
       }
     }

     saveFileDetails(f){
      console.log("Saving file details");
      this.url=APIUrls.hosturl+APIUrls.SaveFileDetails;
      this.http.post(this.url, this.fileDetails)
        .subscribe(data=>{
          this.responseMessage=data;
          this.fileDetails=this.responseMessage.data;
          
          if(this.responseMessage.status==="success" && this.fileDetails !== undefined && this.fileDetails !== null && this.fileDetails.jobHashCode !== null){
            f.submitted=false;
            f.reset();
            this.cancel();
            if(this.action!=undefined && this.action=="Email" ){
              this.getRoles(this.fileDetails.jobHashCode);
              setTimeout(()=>{
                this.showDialog1();
              },500);
            }
            else{
              this.showSuccess(this.responseMessage.message);
              this.router.navigateByUrl("/Content/MasterNumber/MasterNumberDetails/"+this.masterNumberData.mastNumHash);
            }

          }
        },
        (err: HttpErrorResponse) => {
            this.isError=true;
            this.serverErrorMessage = Messages.ServerErrorMessage;
            if (err.error instanceof Error) {
              console.log("Client-side error occured.",err);
            } else {
              console.log("Server-side error occured.",err);
            }
          }
        );
     }

    showSuccess(message) {
        this.msgs = [];
        this.msgs.push({severity:'success', summary:'Success Message', detail:message});
    }

    saveContactData(f){
        if(f.valid){
           let jsonObj = <JSON>this.contactDetails;

      this.url=APIUrls.hosturl+APIUrls.SaveContactDetails;
     
      this.http.post(this.url, this.contactDetails)
        .subscribe(data=>{
          this.responseMessage=data;
          console.log("Successful Saved Data ", this.responseMessage.message);
          if(this.contactDetails.contactType==="Supervisor"){
            this.allSupervisors.push(this.responseMessage.data);
            this.allSupervisors[this.allSupervisors.length-1].fullName=this.allSupervisors[this.allSupervisors.length-1].firstName+" , "+this.allSupervisors[this.allSupervisors.length-1].lastName;
            this.supervisor=this.responseMessage.data;
          }
          else if(this.contactDetails.contactType==="Estimator"){
            this.allEstimators.push(this.responseMessage.data);
            this.allEstimators[this.allEstimators.length-1].fullName=this.allEstimators[this.allEstimators.length-1].firstName+" , "+this.allEstimators[this.allEstimators.length-1].lastName;
            this.estimator=this.responseMessage.data;
          }
          else if(this.contactDetails.contactType==="Coordinator"){
            this.allCoordinators.push(this.responseMessage.data);
            this.allCoordinators[this.allCoordinators.length-1].fullName=this.allCoordinators[this.allCoordinators.length-1].firstName+" , "+this.allCoordinators[this.allCoordinators.length-1].lastName;
            this.coordinator=this.responseMessage.data;
          }
          else if(this.contactDetails.contactType==="Finance"){
            this.financeContacts.push(this.responseMessage.data);
            this.financeContacts[this.financeContacts.length-1].fullName=this.financeContacts[this.financeContacts.length-1].firstName+" , "+this.financeContacts[this.financeContacts.length-1].lastName;
            this.finance=this.responseMessage.data;
          }
          else if(this.contactDetails.contactType==="Sales Representative"){
            this.allSales.push(this.responseMessage.data);
            this.allSales[this.allSales.length-1].fullName=this.allSales[this.allSales.length-1].firstName+" , "+this.allSales[this.allSales.length-1].lastName;
            this.salesRepresentativeName=this.responseMessage.data;
          }
          else if(this.contactDetails.contactType==="Administrator"){
            this.blankRoles1.push(this.responseMessage.data);
            this.blankRoles1[this.blankRoles1.length-1].fullName=this.blankRoles1[this.blankRoles1.length-1].firstName+" , "+this.blankRoles1[this.blankRoles1.length-1].lastName;
            this.Administrator=this.responseMessage.data;
          }
          else if(this.contactDetails.contactType==="Role"){
            this.blankRoles2.push(this.responseMessage.data);
            this.blankRoles2[this.blankRoles2.length-1].fullName=this.blankRoles2[this.blankRoles2.length-1].firstName+" , "+this.blankRoles2[this.blankRoles2.length-1].lastName;
            this.role=this.responseMessage.data;
          }
          
          this.showSuccess(this.responseMessage.message);
          
          f.reset();
          f.submitted=false;
          this.display=false;
        },
        (err: HttpErrorResponse) => {
            this.isError=true;
            this.serverErrorMessage = Messages.ServerErrorMessage;
            if (err.error instanceof Error) {
              console.log("Client-side error occured.",err);
            } else {
              console.log("Server-side error occured.",err);
            }
          }
        );
            //this.contactDetails.companyId=this.masterNumberData.companyId;
        }
         
      }

      cancel(){
        this.displayImageName='';
        for (let i = 0; i < this.imagearray.length;i++ ){
            this.imagearray[i].imageActive=false;
        }
      }

    sendMail(recipients){
      let email: any;
      let content: string;
      if(recipients.errors==null && this.emailTo!=undefined && this.emailTo!="" && this.emailSubject!=undefined && this.emailSubject!=""){
        email={};
        // var element = document.getElementById('emailBody');
        // html2canvas(element).then(function(canvas) {
        //   content=canvas.toDataURL("string");
        //   console.log(content);
        // });
        let content: string;
        content='<html><head></head><body style="padding: 0px;margin: 0px;font-family: Segoe UI,Roboto,Helvetica Neue,Arial,sans-serif,Apple Color Emoji,Segoe UI Emoji,Segoe UI Symbol;">'+
                    '<div class="em_pdf" style="width:98%;padding:0px;margin:10px 1%;float:left;overflow:hidden;-moz-box-shadow: 0 0 5px #888;-webkit-box-shadow: 0 0 5px#888;box-shadow: 0 0 5px #888;">'+
                    '<div class="em_title" style="width:100%;padding:5px 10px;margin:0px;float:left;font-size:16px;letter-spacing:0.5px;background:#595959;color:#FFF;box-shadow: 0 5px 5px -5px #000000;">'+
                    'New File Details</div><div class="em_pdf_info" style="width: 98%;float: left;padding:10px;margin: 0px;overflow: hidden;"><div class="em_row" style="width: 100%;padding: 0px;margin:5px 0px;float: left;overflow: hidden;">'+
                    '<div class="em_row_div" style="width: 25%;padding: 0px;margin:3px 0px;float: left;overflow: hidden;word-wrap: break-word;"><div class="em_text" style="width: 100%;padding: 0px;margin: 0px;float: left;overflow: hidden;font-size: 12px;color: #737373;letter-spacing: 0.5px;">'+
                    'File Type</div><div class="em_value" style="width: 100%;padding: 0px;margin: 0px;float: left;color: #666666; overflow: hidden;font-size: 13px;font-weight: bold;word-wrap: break-word;">'+this.fileDetails.jobType+'</div></div></div></div>'+
                    '<div class="em_title" style="width:100%;padding:5px 10px;margin:0px;float:left;font-size:16px;letter-spacing:0.5px;background:#595959;color:#FFF;box-shadow: 0 5px 5px -5px #000000;">Roles</div>'+
                    '<div class="em_pdf_info" style="width:99%;padding-left:10px;padding-top:10px;padding-bottom:10px;float: left;margin: 0px;overflow: hidden;"><div class="contact_table" style="width: 100%;padding: 0px;margin: 0px;float: left;overflow: hidden;">'+
                    '<table style="width: 99%;padding: 0px;margin: 0px 1% 0px 0px;float: left;border-collapse: collapse;border: 1px solid #CCC;box-sizing: border-box;"><thead>'+
                    '<tr><th style="background: #CCC;padding: 5px 10px;color: #595959;font-size: 13px;font-weight: 500;">Name</th><th style="background: #CCC;padding: 5px 10px;color: #595959;font-size: 13px;font-weight: 500;">Role</th>'+
                    '<th style="background: #CCC;padding: 5px 10px;color: #595959;font-size: 13px;font-weight: 500;">Email</th><th style="background: #CCC;padding: 5px 10px;color: #595959;font-size: 13px;font-weight: 500;">Phone Number</th></tr></thead><tbody>';
        for(let role of this.roles){
            content+='<tr><td style="padding: 3px 5px;border: 1px solid #CCC;box-sizing: border-box;font-size: 12px;letter-spacing: 0.5px;color: #595959;">'+role.name+'</td>'+
            '<td style="padding: 3px 5px;border: 1px solid #CCC;box-sizing: border-box;font-size: 12px;letter-spacing: 0.5px;color: #595959;">'+role.role+'</td>'+
            '<td style="padding: 3px 5px;border: 1px solid #CCC;box-sizing: border-box;font-size: 12px;letter-spacing: 0.5px;color: #595959;">';
            if(role.email !== null && role.email !== undefined){
              content+=role.email;
            }
            content+='</td><td style="padding: 3px 5px;border: 1px solid #CCC;box-sizing: border-box;font-size: 12px;letter-spacing: 0.5px;color: #595959;">';
            if(role.phoneNo !== null && role.phoneNo !== undefined){
              content+=role.phoneNo;
            }
            content+='</td></tr>';                                
        }  
        content+='</tbody></table></div></div></div><div class="sign-div" style="width: 98%;float: left;padding: 0px;margin: 0px 1%;"><div class="sign-thanks" style="width: 100%; font-size: 16px;color: #666666;float: left;font-weight: 500;letter-spacing: 0.3px;">Thanks & Regards,</div><div class="sign-name" style="width: 100%;font-size: 20px;color: #333333;float: left;font-weight: 600;letter-spacing: 0.3px;">Xcelerate Team.</div></div><div class="discalim" style="width:98%;float: left;border-top:1px solid #333333;box-sizing: border-box;padding: 5px 0px;font-size: 12px;text-align: justify;color: #666666;margin:8px 1% 10px 1%;">"This email and any files transmitted with it are confidential and intended solely for the use of the individual or entity to whom they are addressed. If you have received this email in error please notify the system manager. This message contains confidential information and is intended only for the individual named. If you are not the named addressee you should not disseminate, distribute or copy this e-mail. Please notify the sender immediately by e-mail if you have received this e-mail by mistake and delete this e-mail from your system. If you are not the intended recipient you are notified that disclosing, copying, distributing or taking any action in reliance on the contents of this information is strictly prohibited."</div></body></html>';          
        email.to=this.emailTo;
        email.subject=this.emailSubject;
        email.source=this.emailSource;
        email.sourceId=this.fileDetails.jobId;
        email.mailOption=this.emailOption;
        email.message=content;
        console.log(email);
        this.url=APIUrls.hosturl+APIUrls.SendEmail;
      setTimeout(()=>{
       email.message=content;
       console.log(email);
       this.http.post(this.url, email)
        .subscribe(data=>{
          this.myData=data;
          this.showSuccess(this.myData.message);
          this.router.navigateByUrl("/Content/MasterNumber/MasterNumberDetails/"+this.masterNumberData.mastNumHash);
        },
        (err: HttpErrorResponse) => {
            this.isError=true;
            this.serverErrorMessage = Messages.ServerErrorMessage;
            if (err.error instanceof Error) {
              console.log("Client-side error occured.",err);
            } else {
              console.log("Server-side error occured.",err);
            }
          }
        )},1000);
        this.displayEmail=false;
      }
      else if(this.emailTo==undefined || this.emailTo==""){
        this.showMailToMsg=true;
      }
      else if(this.emailSubject==undefined || this.emailSubject==""){
        this.showSubjectMsg=true;
      }
    }

    sendFileDetails(f){
        if(f.valid){
          this.action="Email";
          this.saveData(f);
        }
        else{
          f.submitted=true;
        }
    }

     getRoles(jobHash){
      let getParams = new HttpParams().set('jobHashCode', jobHash);
      this.url =APIUrls.hosturl+APIUrls.FileRoles;
      this.http.get(this.url,{params:getParams})
      .subscribe(data=>{
          this.myData = data;
          this.roles=this.myData.roles;
      },
      (err: HttpErrorResponse) => {
          this.isError=true;
          this.serverErrorMessage = Messages.ServerErrorMessage;
            if (err.error instanceof Error) {
              console.log("Client-side error occured.");
            } else {
              console.log("Server-side error occured.");
            }
          }
      );
    }

}
