import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';

import { DocumentDetails } from '../../../models/document';
import { APIUrls } from '../../../shared/constants/apiurls';
import { HttpClient, HttpHeaders, HttpParams, HttpErrorResponse } from '@angular/common/http';
import { DocumentsInfo } from '../../../models/documents';

@Component({
  selector: 'subjob-documents',
  templateUrl: './subjobs-documents.component.html',
  styleUrls: ['./subjobs-documents.component.css']
})


export class SubJobsDocumentsComponent implements OnInit {
  document: any;
  file: any;
  url: string;
  myData: any;
  selectedDocuments: any;
  showMessage: boolean=false;
  fileMessage: boolean=true;
  checkAll: boolean=false;
  docs: any;
  @Input()
  set documents(documents: DocumentsInfo[]){
    this.docs=documents;
    if(this.docs!=null)
      for(let doc of this.docs){
        doc.selected=false;
      }
  }
  //@Input() documents: DocumentsInfo[]; 
  @Output() addDocument = new EventEmitter<any>();
  @Output() deletedDocuments = new EventEmitter<any>();
  @ViewChild('file')
  doc: any;

  display2:boolean = false;
  showDialog2(){
    this.display2 = true;
  }

  constructor(private http : HttpClient) {
  }
 
  display: boolean = false;
    showDialog(f) {
      f.reset();
      f.submitted=false;
      this.showMessage = false;
      this.display = true;
  }

  ngOnInit(): void {
    this.document={};
    this.selectedDocuments=[];
  }

    saveDocument(f){
      //console.log(this.file);
      if(f.valid && this.file!=undefined && this.file!=""){
        this.addDocument.emit({"document":this.document,"file":this.file});
        this.display=false;
        f.submitted=false;
        f.reset();
        this.doc.nativeElement.value = "";
        this.file="";
      }
      else{
        if(this.file==undefined || this.file==""){
            this.fileMessage=false;
        }
      }
    }

    reset(f){
      f.reset();
      this.display=false;
      this.doc.nativeElement.value = "";
    }

    onChange(files){
      this.file=files[0];
      if(files.length>0){
        this.fileMessage=true;
      }
      else{
        this.fileMessage=false;
      }
    }

    selectAll(event){
      this.selectedDocuments=[];
      if(event.target.checked) {
        this.checkAll=true;
        for(let doc of this.docs){
          doc.selected=true;
           this.selectedDocuments.push(doc.id);
        }
      }
      else{
        this.checkAll=false;
        for(let doc of this.docs){
          doc.selected=false;
        }
        
      }
    }

    onCheckboxChange(document, event) {
        if(event.target.checked) {
          this.showMessage=false;
          document.selected=true;
          this.selectedDocuments.push(document.id);
          if(this.docs.length===this.selectedDocuments.length){
            this.checkAll=true;
          }
          else{
            this.checkAll=false;
          }
        } else {
          document.selected=false;
          this.checkAll=false;
          for(var i=0 ; i < this.selectedDocuments.length; i++) {
            if(this.selectedDocuments[i] == document.id){
              this.selectedDocuments.splice(i,1);
            }
          }
        }
        //console.log(this.selectedDocuments);
    }

    deleteDocuments(){
      if(this.selectedDocuments!=undefined && this.selectedDocuments.length>0){
        this.showMessage=false;
        this.deletedDocuments.emit({"selDocs": this.selectedDocuments});
        this.selectedDocuments=[];
      }
      else{
        this.showMessage=true;
      }
    }

    downloadDocument(document){
      // let fileId=document.id+"";
      // let getParams = {"fileId": fileId.toString()};
      // this.url =APIUrls.hosturl+APIUrls.DownloadFileDocument;
      // console.log(getParams);
      // this.http.get(this.url,{params:getParams,responseType:'arraybuffer'})
      // .subscribe(data=>{
         
      // },
      // (err: HttpErrorResponse) => {
      //       if (err.error instanceof Error) {
      //         console.log("Client-side error occured.");
      //       } else {
      //         console.log(HttpErrorResponse);
      //         console.log("Server-side error occured.");
      //       }
      //     }
      // );
       var downloadPath = APIUrls.hosturl+APIUrls.DownloadFileDocument+"?fileId="+document.id;
       window.open(downloadPath, '_blank', '');
    }
}
