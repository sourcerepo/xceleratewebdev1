import { Component, OnInit } from '@angular/core';

import {HttpClient,HttpHeaders, HttpParams, HttpErrorResponse} from '@angular/common/http';
import {Router, ActivatedRoute} from '@angular/router';

import {SubJobsGrid} from '../../../models/subjobs-grid';
import {APIUrls} from '../../../shared/constants/apiurls';
import {Messages} from '../../../shared/constants/messages';
import {FilesPath} from '../../../shared/constants/filespath';
import {LocationService} from '../../../shared/services/location.service';

@Component({
  selector: 'subjob-grid',
  templateUrl: './subjobs-grid.component.html',
  styleUrls: ['./subjobs-grid.component.css']
})


export class SubJobsGridComponent implements OnInit {
subJobs: SubJobsGrid[];
dtOptions: DataTables.Settings = {};
myData:any;
jobs: SubJobsGrid[];
isError:boolean;
serverErrorMessage:string;
jobImagepath:string = FilesPath.JOBTYPE_ICONS ;
url:string;
private sub: any;
private id;
locationHash: string;

filesfil:any[];
statusFilters: any[];
estimateFilters: any[];
supervisorFilters: any[];
estimatorFilters: any[];
cols: any[];
status: string[];
estimate: string[];
supervisor: string[];
estimator: string[];
fileNumber: string;
customerName: string;
fileType: string[];

display: boolean = false;
    showDialog() {
        this.display = true;
    }
    
  constructor(private http: HttpClient, private router: Router, private route: ActivatedRoute, private userLocation:LocationService) {
        this.cols = [
                { field: 'status', header: 'Status', showData:true},
                { field: 'subJobCode', header: 'File Number',showData:true },
                { field: 'jobImage', header: 'File Details',showData:true},
                { field: 'customerName', header: 'Customer Name', showData:true },
                { field: 'lossAddress', header: 'Loss Address', showData:true},
                { field: 'city', header: 'City', showData:true},
                { field: 'state', header: 'State', showData:true},
                { field: 'zip', header: 'Zip', showData:true},
                { field: 'supervisor', header: 'Supervisor', showData:true},
                { field: 'estimator', header: 'Estimator', showData:true},
                { field: 'createdDatetime', header: 'Created Date & Time',showData:true },
                {field: 'jobType',header: 'File Type',showData:false },
                { field: 'estimate', header: 'Estimate', showData:true},
                {field: 'delete',header: 'Delete',showData:true },
            ];

          
  }

  getSubJobs(){
        //this.url =APIUrls.hosturl+APIUrls.FilesGrid;
        let element: HTMLElement = document.getElementById("reset") as HTMLElement;
        element.click();
        this.subJobs=[];
        this.supervisorFilters=[];
        this.estimatorFilters=[];
        if(this.locationHash!==undefined && this.locationHash!=null){
            let getParams = new HttpParams().set('locationHash', this.locationHash);
            this.http.get(this.url,{params:getParams})
            .subscribe(data=>{
            this.myData=data;
            this.subJobs=this.myData.jobs;
            if(this.subJobs!=null){
                for (let file of this.subJobs) {
                    file.style=file.status.toLowerCase().replace(/ /g,"-");
                    file.jobImage=this.jobImagepath+file.jobType+".png";
                    if(file.supervisor !== null && this.supervisorFilters.indexOf("{label: '"+file.supervisor+"', value: '"+file.supervisor+"'}")<0){
                        this.supervisorFilters.push({label: file.supervisor, value:file.supervisor});
                    }
                    if(file.estimator !== null && this.estimatorFilters.indexOf("{label: '"+file.estimator+"', value: '"+file.estimator+"'}")<0){
                        this.estimatorFilters.push({label: file.estimator, value:file.estimator});
                    }
                }
            }
            },
            (err: HttpErrorResponse) => {
                    this.isError=true;
                    this.serverErrorMessage = Messages.ServerErrorMessage;
                    if (err.error instanceof Error) {
                            console.log("Client-side error occured.");
                    } else {
                            console.log("Server-side error occured.");
                    }
            }
            );
        }
    }
 
  ngOnInit() {
        
        this.sub = this.route.params.subscribe(params => {
            this.id = params['id'];
            if (this.id=='my'){     
                this.url =APIUrls.hosturl+APIUrls.MyFilesGrid;
            }else if(this.id=='all'){
                this.url =APIUrls.hosturl+APIUrls.FilesGrid; 
            } 
            else{
                this.url =APIUrls.hosturl+APIUrls.AttentionRequiredFilesGrid;    
            }
            this.getSubJobs();
        });
        this.userLocation.currentLocation.subscribe(value => {
            this.locationHash=value.hash;
            this.getSubJobs();
        });

        this.filesfil =[
            {label: 'Water', value: 'Water'},
            {label: 'Reconstruction', value: 'Reconstruction'},
            {label: 'Content', value: 'Content'},
            {label: 'Mold', value: 'Mold'},
            {label: 'Asbestos', value: 'Asbestos'},
            {label: 'Board Up', value: 'Board Up'},
            {label: 'Consulting', value: 'Consulting'},
            {label: 'Bio', value: 'Bio'}
         ];

         this.statusFilters=[
            {label: 'New', value: 'New'},
            {label: 'Estimate', value: 'Estimate'},
            {label: 'Sales', value: 'Sales'},
            {label: 'Planing', value: 'Planing'},
            {label: 'Production', value: 'Production'},
            {label: 'Invoice', value: 'Invoice'},
            {label: 'Closed', value: 'Closed'},
            {label: 'Rejected', value: 'Rejected'}
         ];
        
         this.estimateFilters=[
            {label: 'Yes', value: 'Yes'},
            {label: 'No', value: 'No'},
         ];


    }

    reset(dt1){
        this.fileNumber="";
        this.customerName="";
        this.status=[];
        this.fileType=[];
        this.estimator=[];
        this.supervisor=[];
        this.estimate=[];
        dt1.reset();
    }

}
