
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { APIUrls } from '../constants/apiurls';
import { ConfigurationData } from '../../models/config-data';

@Injectable()
export class ConfigurationDataService  {
    private url:string;
    private myData:any;
    constructor (private http: HttpClient) {
      
    }
    
    getConfigData(){
      this.url =APIUrls.hosturl+APIUrls.MasterNumberConfigurationData;
      return this.http.get(this.url).map(data => {
        this.myData=data;
        return this.myData.configData;
      });
    }
}