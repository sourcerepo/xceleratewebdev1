export class MasterNumberData{
   mastNumId: number;
   actualMastNumId: string;
   catCode: string;
   createdBy: number;
   createdDatetime: string;
   customerId: number;
   customerName: String;
   address: string;
   phoneNo:string;
   lossAddress: string;
   billingAddress: string;
   city: string;
   zip: string;
   damageType: string;
   duplicateComments: string;
   duplicateEnteredBy: number;
   duplicateEnteredDatetime: string;
   isDuplicate: string;
   isInsured: string;
   mastNumCode: string;
   mastNumDesc: string;
   mastNumHash: string;
   propertyType: string;
   restCompLocationId: number;
   restorationType: string;
   sourceId: number;
   sourceType: number;
   programType: string;
   causeOfLoss: string;
   yearBuilt: string;
   status: string;
   updatedBy: number;
   updatedDatetime: string

}