export class MainCustomers{
    customerName:string;
    address:string;
    city:string;
    state:string;
    zip:string;
    phoneNumber:string;
    activity:string;
    //jobList:string;
    // jobImage:string;
    // jobType:string;
    status:string;
    propertyType:string;
    salesRepresentative:string;
}