export class AllTasksList {
    title:string;
    subJobCode:string;
    status:string;
    dueDate:string;
    jobImage:string;
    responsible:string;
}