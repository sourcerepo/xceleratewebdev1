export class ContactDetails {
    id:number;
    firstName: string;
    lastName: string;
    phoneNo:string;
    mobile:string;
    address:string;
    contactType:string;
    status:string;
    fullName:string;
    city:string;
    state:string;
    zip: string;
    email: string;
    companyId: string;
    companyName: string;
  }