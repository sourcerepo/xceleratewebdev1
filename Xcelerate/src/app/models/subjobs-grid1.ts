export interface SubJobsGrid{
      SubJobCode:string,
      CustomerName:string,
      LossAddress:string,
      City:string;
      State:string;
      Zip:string;
      JobType:string,
      JobImage:string,
      Status:string,
      //ProjectManager:string,
      Supervisor:string;
      Estimator:string;
      CreatedDTTM:string;
      Style:string;
}