export class DocumentDetails {
    name:string;
    type: string;
    category: string;
    comments: string;
    file: File;
}