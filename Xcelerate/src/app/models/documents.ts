export class DocumentsInfo {
    id: number;
    hash: string;
    name: string;
    type: string;
    comments: string;
    uploadedBy: string;
    uploadedDatetime: Date;
  }