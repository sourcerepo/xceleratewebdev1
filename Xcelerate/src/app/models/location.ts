export class LocationDetails {
    id: number;
    code: string;
    hash: string;
    address: string;
    city: string;
    state: string;
    zip: string;
    description: string;
}