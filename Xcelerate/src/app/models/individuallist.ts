export class IndividualList{
    name:string;
    jobreferred:string;
    amountbids:string;
    amountsales:string;
    invested:string;
    spent:string;
    classification:string;
    salesrep:string;
}