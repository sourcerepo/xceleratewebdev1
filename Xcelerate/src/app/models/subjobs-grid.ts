export interface SubJobsGrid{
      subJobCode:string;
      customerName:string;
      lossAddress:string;
      city:string;
      state:string;
      zip:string;
      jobType:string;
      jobImage:string;
      status:string;
      jobHashCode:string;
      //ProjectManager:string,
      supervisor:string;
      estimator:string;
      createdDatetime:string;
      style:string;
}